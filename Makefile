
# name des programms
EXECUTABLE=gnarf

# compiler
CC=gcc

# shredder
RM=rm

# make dir
MKDIR=mkdir

# hexdump tool
HEXDUMP=hexdump -C

# compiler argumente
CFLAGS=-Iinclude -ggdb3 -O0 -c -Wall

# linker argumente
LDFLAGS=-ggdb3 -O0

BIN_DIR=bin
SRC_DIR=src

# alle .c dateien im verzeichnis
SOURCES=$(shell ls src/*.c)

# alle .o dateien
OBJECTS=$(SOURCES:.c=.o)

all:		$(SOURCES) $(BIN_DIR) $(BIN_DIR)/$(EXECUTABLE)

clean:
	$(RM) -f test.gnarf test.dump
	$(RM) -f $(BIN_DIR)/$(EXECUTABLE) $(BIN_DIR)/$(EXECUTABLE).exe $(OBJECTS)
	$(RM) -rf $(BIN_DIR)
	
test:		all
	$(RM) -f test.gnarf test.dump
	$(BIN_DIR)/$(EXECUTABLE) -n test.gnarf
	$(BIN_DIR)/$(EXECUTABLE) -d test.gnarf
	$(BIN_DIR)/$(EXECUTABLE) -a test.gnarf ./README.md
	$(BIN_DIR)/$(EXECUTABLE) -a test.gnarf ./LICENSE.txt
	$(BIN_DIR)/$(EXECUTABLE) -d test.gnarf
	$(HEXDUMP) test.gnarf > test.dump

$(BIN_DIR):
	$(MKDIR) $@
	
$(BIN_DIR)/$(EXECUTABLE):	$(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.PHONY:   all clean test
