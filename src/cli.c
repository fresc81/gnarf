/*
 *  This file is part of gnarf.
 *
 *  gnarf is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  gnarf is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with gnarf.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include <stdio.h>
#include <stdlib.h>

#include <getopt.h>

#include "gnarf.h"

static struct option g_oLongOptions[] =
{
  {"new", required_argument, 0, 'n'},
  {"dir", required_argument, 0, 'd'},
  {"add", required_argument, 0, 'a'},
  {0, 0, 0, 0}
};

int main(int argc, char *argv[])
{
  int iParameter;
  int iOptionIndex;

  // most of this code is taken from getopt example http://www.gnu.org/software/libc/manual/html_node/Getopt-Long-Option-Example.html#Getopt-Long-Option-Example
  
  iOptionIndex = 0;
  iParameter = getopt_long(argc, argv, "n:d:a:", g_oLongOptions, &iOptionIndex);
  
  /* Detect the end of the options. */
  if (iParameter != -1)
  {
    switch (iParameter)
    {
    
    case 'n':
      gnarf_new(optarg);
      break;
    
    case 'd':
      gnarf_dir(optarg);
      break;
    
    case 'a':
      while (optind < argc)
        gnarf_add(optarg, argv[optind++]);
      break;
    
    case '?':
      /* getopt_long already printed an error message. */
      break;
    
    default:
      abort ();
      
    }
  }

  return 0;
}
