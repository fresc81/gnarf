/*
 *  This file is part of gnarf.
 *
 *  gnarf is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  gnarf is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with gnarf.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <libgen.h>

#include "gnarf.h"


/// maximum number of chars for filenames
#define GNARF_MAX_FILENAME  256

/// number of index entries per block
#define GNARF_NUM_INDICES   16

/// magic value to detect archive corruption
#define GNARF_MAGIC_VALUE   0x4242


#ifndef min
#define min(a,b) (((a) < (b)) ? (a) : (b))
#endif


/// type that represents the magic value
typedef uint16_t GNARF_MAGIC;


/**
 * type that represents the status of an index entry
 */
typedef enum GNARF_STATUS {

  /// the entry is free
  GS_FREE,
  
  /// the entry contains valid data
  GS_ACTUAL,
  
  /// the enty has been deleted
  GS_DELETED,
  
  /// the entry has been replaced by another entry
  GS_REPLACED,
  
  /// the entry is the last in the block and archive
  GS_EOA,
  
  /// the entry is the last in the block but not in the archive
  GS_CONTINUE
  
} GNARF_STATUS;


/// type that represents a user id
typedef uint32_t GNARF_UID;


/// type that represents a group id
typedef uint32_t GNARF_GID;


/**
 * type that represents the type of an index entry
 */
typedef enum GNARF_TYPE {

  /// the entry is a directory
  GT_DIRECTORY,
  
  /// the entry is a file
  GT_FILE

} GNARF_TYPE;


/// type that stores a filename with a static number of chars
typedef char GNARF_FILENAME[GNARF_MAX_FILENAME];


/// unsigned integer type that stores the size of a file
typedef size_t GNARF_SIZE;


/// unsigned integer type that stores the position of a file
typedef size_t GNARF_POSITION;


/**
 * type that represents an index entry
 */
typedef struct GNARF_INDEX {

  /// the status
  GNARF_STATUS    m_eStatus     __attribute__ ((packed));
  
  /// the user id
  GNARF_UID       m_uUserId     __attribute__ ((packed));
  
  /// the group id
  GNARF_GID       m_uGroupId    __attribute__ ((packed));
  
  /// the entry type
  GNARF_TYPE      m_eType       __attribute__ ((packed));
  
  /// the filename
  GNARF_FILENAME  m_csFilename;
  
  /// the length
  GNARF_SIZE      m_szLength    __attribute__ ((packed));
  
  /// the position in the file
  GNARF_POSITION  m_szPosition  __attribute__ ((packed));

} GNARF_INDEX, *PGNARF_INDEX;


/// type that stores a static number of index entries
typedef GNARF_INDEX GNARF_INDICES[GNARF_NUM_INDICES];


/**
 * type that represents a block of indices
 */
typedef struct GNARF_BLOCK
{
  
  /// the magic value
  GNARF_MAGIC     m_uMagic      __attribute__ ((packed));
  
  /// the indices
  GNARF_INDICES   m_ssIndices;
  
} GNARF_BLOCK, *PGNARF_BLOCK;


/***************\
| private stuff |
\***************/


/**
 * Reads a block of indices and stores it to the location pointed to by <code>pIndexBlock</code>.
 * 
 * \param fdArchiveFile
 *  a file descriptor that points to an previously opened archive file
 * 
 * \param pIndexBlock
 *  a pointer to the variable that receives the data that has been read from the file
 * 
 * \return
 *  on success a value of GE_NO_ERROR (0) is returned, otherwise a value indicating the
 *  error that had occured is returned.
 * 
 */
static GNARF_ERROR gnarf_read_block(int fdArchiveFile, PGNARF_BLOCK pIndexBlock)
{
  GNARF_ERROR eResult;    // return value
  ssize_t szOverallRead;  // counts the number of bytes read
  ssize_t szActualRead;   // stores the return value of 'read()'
  char *pPosition;
  
  if (pIndexBlock != NULL)
  {
    pPosition = (char*) pIndexBlock;  // point to beginning of data
    szOverallRead = 0;                // reset counter
    
    // reset memory
    memset(pIndexBlock, 0, sizeof(GNARF_BLOCK));
    
    // read until all bytes have been come available...
    while ((szActualRead = read(fdArchiveFile, pPosition, sizeof(GNARF_BLOCK)-szOverallRead)) > 0)
    {
      // count bytes read
      szOverallRead += szActualRead;
      // advance data pointer
      pPosition += szActualRead;
    }
    
    // if everything has been read or on EOF read returns 0, on IO error read returns -1
    if (szActualRead == -1)
      eResult = GE_IO_ERROR;
    else // szActualRead == 0 -> everything read or EOF
    {
      
      // check if EOF came before end of this block
      if (szOverallRead != sizeof(GNARF_BLOCK))
        eResult = GE_UNEXPECTED_EOF;
      else if (pIndexBlock->m_uMagic != GNARF_MAGIC_VALUE) // validate magic
        eResult = GE_MAGIC_VALUE_CORRUPT;
      else
        eResult = GE_NO_ERROR;
      
    }
    
  } else
  {
    // don't know where to store data
    eResult = GE_NULL_POINTER_ERROR;
  }
  
  return eResult;
}


/**
 * Writes the block of indices pointed to by <code>pIndexBlock</code> to the file.
 * 
 * \param fdArchiveFile
 *  a file descriptor that points to an previously opened archive file
 * 
 * \param pIndexBlock
 *  a pointer to the variable that contains the data that should be written to the file
 * 
 * \return
 *  on success a value of GE_NO_ERROR (0) is returned, otherwise a value indicating the
 *  error that had occured is returned.
 * 
 */
static GNARF_ERROR gnarf_write_block(int fdArchiveFile, PGNARF_BLOCK pIndexBlock)
{
  GNARF_ERROR eResult;        // return value
  ssize_t szOverallWritten;   // counts the number of bytes written
  ssize_t szActualWritten;    // stores the return value of 'write()'
  char *pPosition;
  
  if (pIndexBlock != NULL)
  {
    pPosition = (char*) pIndexBlock;  // point to beginning of data
    szOverallWritten = 0;             // reset counter
    
    // ensure the magic value is properly set
    pIndexBlock->m_uMagic = GNARF_MAGIC_VALUE;
    
    // write until all bytes have been consumed...
    while ((szActualWritten = write(fdArchiveFile, pPosition, sizeof(GNARF_BLOCK)-szOverallWritten)) > 0)
    {
      // count bytes written
      szOverallWritten += szActualWritten;
      // advance data pointer
      pPosition += szActualWritten;
    }
    
    // if everything has been written write returns 0, on IO error write returns -1
    if (szActualWritten == -1)
      eResult = GE_IO_ERROR;
    else // szActualWritten == 0 -> everything written
    {
      
      // flush data downstream...
      if (fsync(fdArchiveFile) == -1)
        eResult = GE_IO_ERROR;
      else
        eResult = GE_NO_ERROR;
        
    }
    
  } else
  {
    // don't know where to get data from
    eResult = GE_NULL_POINTER_ERROR;
  }
  
  return eResult;
}


/**
 * Copies a given number of bytes from the input file into the output file. It tries to copy byte
 * blocks of the given block size until all bytes have been written.
 * 
 * \param fdOutputFile
 *  
 * \param fdInputFile
 *  
 * \param szNumBytes
 *  
 * \param szBlkSize
 *  
 * \return
 *  
 */
static GNARF_ERROR gnarf_copy_data(int fdOutputFile, int fdInputFile, size_t szNumBytes, size_t szBlkSize)
{
  ssize_t szRead;
  size_t szBlkRead;
  ssize_t szWrite;
  size_t szBlkWrite;
  GNARF_ERROR eResult = GE_NO_ERROR;
  char *pcBuffer = (char*) alloca(szBlkSize);
  char *pcPosition;
  
  do
  {
    
    // read block
    pcPosition = pcBuffer;
    szBlkRead  = 0;
    while ((szRead = read(fdInputFile, pcPosition, min(szBlkSize-szBlkRead, szNumBytes))) > 0)
    {
      szBlkRead  += szRead;
      pcPosition += szRead;
    }
    
    if (szRead < 0)
      eResult = GE_IO_ERROR;
    else
    {
      
      // write block
      pcPosition = pcBuffer;
      szBlkWrite = 0;
      while ((szWrite = write(fdOutputFile, pcPosition, szBlkRead-szBlkWrite)) > 0)
      {
        szNumBytes -= szWrite;
        szBlkWrite += szWrite;
        pcPosition += szWrite;
      }
      
      if (szWrite < 0)
        eResult = GE_IO_ERROR;
      
    }
    
  } while (!eResult && (szNumBytes > 0));
  
  return eResult;
}


/*************\
| API exports |
\*************/


/**
 * Create a new empty archive file with the given filename.
 * 
 * \param pcArchiveName
 * 
 * \return
 * 
 */
GNARF_ERROR gnarf_new(const char *pcArchiveName)
{
  GNARF_ERROR eResult;
  GNARF_BLOCK sBlock;
  int fdArchiveFile = creat(pcArchiveName, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
  
  if (fdArchiveFile != -1)
  {
    // initialize block data with zeros, so all elements get their default values
    memset(&sBlock, 0, sizeof(GNARF_BLOCK));
    
    // set status field of last index entry
    sBlock.m_ssIndices[GNARF_NUM_INDICES - 1].m_eStatus = GS_EOA;
    
    // write to file
    eResult = gnarf_write_block(fdArchiveFile, &sBlock);
    
    if (!eResult)
      printf("new archive '%s' created.\n", pcArchiveName);
    
    close(fdArchiveFile);
    
  } else
  {
    eResult = GE_UNABLE_TO_CREATE_ARCHIVE;
  }
  
  return eResult;
}


/**
 * List the contents of the given archive file.
 * 
 * \param pcArchiveName
 * 
 * \return
 * 
 */
GNARF_ERROR gnarf_dir(const char *pcArchiveName)
{
  GNARF_ERROR eResult;
  GNARF_BLOCK sBlock;
  PGNARF_INDEX pIndex;
  size_t uIndexId;
  int bEndOfArchive;
  int fdArchiveFile = open(pcArchiveName, O_RDONLY);
  
  if (fdArchiveFile != -1)
  {
    printf("listing contents of '%s'\n", pcArchiveName);
    
    do // until end of archive or error
    {
      eResult = gnarf_read_block(fdArchiveFile, &sBlock);
      if (!eResult)
      {
        printf("%-8s %s\n", "Size", "Filename");
        for (uIndexId = 0; uIndexId < GNARF_NUM_INDICES; uIndexId++)
        {
          pIndex = &(sBlock.m_ssIndices[uIndexId]);
          printf("Index (%zu) Status: %d\n", uIndexId, pIndex->m_eStatus);
          
          switch (pIndex->m_eStatus)
          {
            
          case GS_ACTUAL:
            printf("%8zu %s\n", pIndex->m_szLength, pIndex->m_csFilename);
            break;
            
          default:
            break;
            
          }
          
        }
        
        switch (sBlock.m_ssIndices[GNARF_NUM_INDICES - 1].m_eStatus)
        {
        case GS_CONTINUE:
          // set position in file to beginning of next block
          if (lseek(fdArchiveFile, sBlock.m_ssIndices[GNARF_NUM_INDICES - 1].m_szPosition, SEEK_CUR) == -1)
            eResult = GE_IO_ERROR;
          else
            bEndOfArchive = 0;
          break;
          
        case GS_EOA:
          bEndOfArchive = 1;
          break;
          
        default:
          eResult = GE_ARCHIVE_INDEX_CORRUPT;
          break;
        }
        
      }
    } while (!eResult && !bEndOfArchive);
    
    close(fdArchiveFile);
    
  } else
  {
    eResult = GE_UNABLE_TO_OPEN_ARCHIVE;
  }
  return eResult;
}

/**
 * Adds the given input file to the archive file.
 * 
 * \param pcArchiveName
 * 
 * \param pcInputName
 * 
 * \return
 * 
 */
GNARF_ERROR gnarf_add(const char *pcArchiveName, const char *pcInputName)
{
  GNARF_ERROR eResult;
  GNARF_BLOCK sBlock;
  struct stat sStat;
  // memory for a copy of the path, alloca doesn't need to be free'd
  char *pcCopyInputName = (char*) alloca(strlen(pcInputName) + 1);
  char *pcFilename;
  size_t uIndexId;
  size_t uBlockPos;
  size_t uNextData;
  int bEndOfArchive;
  int bFoundFreeBlock;
  int fdArchiveFile = open(pcArchiveName, O_RDWR);
  int fdInputFile = open(pcInputName, O_RDONLY);
  
  // get the filename part of the path
  strcat(pcCopyInputName, pcInputName);
  pcFilename = basename(pcCopyInputName);
  
  if (fdArchiveFile != -1)
  {
    if (fdInputFile != -1)
    {
      /* --- BEGIN find the first free index --- */
      uNextData = sizeof(GNARF_BLOCK); // offset of the data within the file
      bFoundFreeBlock = 0;
      do // until end of archive or free block found or error
      {
        
        // get current position
        uBlockPos = lseek(fdArchiveFile, 0, SEEK_CUR);
        if (uBlockPos == -1)
          eResult = GE_IO_ERROR;
        else
          eResult = gnarf_read_block(fdArchiveFile, &sBlock);
        
        if (!eResult)
        {
          
          for (uIndexId = 0; (uIndexId < GNARF_NUM_INDICES) && !bFoundFreeBlock; uIndexId = bFoundFreeBlock ? uIndexId : uIndexId + 1)
          {
            
            // TODO: check if this input would overwrite an existing entry
            
            if (sBlock.m_ssIndices[uIndexId].m_eStatus == GS_FREE)
              bFoundFreeBlock = 1;
            else if (sBlock.m_ssIndices[uIndexId].m_eStatus != GS_CONTINUE)
              uNextData = sBlock.m_ssIndices[uIndexId].m_szPosition + sBlock.m_ssIndices[uIndexId].m_szLength;
          }
          
          if (!bFoundFreeBlock)
          {
            
            switch (sBlock.m_ssIndices[GNARF_NUM_INDICES - 1].m_eStatus)
            {
            case GS_CONTINUE:
              // set position in file to beginning of next block
              if (lseek(fdArchiveFile, sBlock.m_ssIndices[GNARF_NUM_INDICES - 1].m_szPosition, SEEK_CUR) == -1)
                eResult = GE_IO_ERROR;
              else
                bEndOfArchive = 0;
              break;
              
            case GS_EOA:
              bEndOfArchive = 1;
              break;
              
            default:
              eResult = GE_ARCHIVE_INDEX_CORRUPT;
              break;
            }
            
          }
          
        }
      } while (!eResult && !bEndOfArchive && !bFoundFreeBlock);
      /* --- END find the first free index --- */
      
      /* --- BEGIN aquire the found index and attach the file --- */
      if (!eResult)
      {
        if (bFoundFreeBlock)
        {
          printf("found free block at 0x%zx\n", uNextData);
          
          // set file position to begin of data
          if (lseek(fdArchiveFile, (ssize_t) uNextData, SEEK_SET) == -1)
            eResult = GE_IO_ERROR;
          else
          {
            
            // get length of the input file
            memset(&sStat, 0, sizeof(struct stat));
            if (fstat(fdInputFile, &sStat) == -1)
              eResult = GE_IO_ERROR;
            else
            {
              printf("'%s' filesize is %zd, filename is '%s'\n", pcInputName, sStat.st_size, pcFilename);
              
              // copy data
              eResult = gnarf_copy_data(fdArchiveFile, fdInputFile, sStat.st_size, sStat.st_blksize);
              if (!eResult)
              {
                
                // set file position to begin of block
                if (lseek(fdArchiveFile, (ssize_t) uBlockPos, SEEK_SET) == -1)
                  eResult = GE_IO_ERROR;
                else
                {
                  // update block
                  sBlock.m_ssIndices[uIndexId].m_eStatus    = GS_ACTUAL;
                  sBlock.m_ssIndices[uIndexId].m_uUserId    = sStat.st_uid;
                  sBlock.m_ssIndices[uIndexId].m_uGroupId   = sStat.st_gid;
                  sBlock.m_ssIndices[uIndexId].m_eType      = S_ISDIR(sStat.st_mode) ? GT_DIRECTORY : GT_FILE;
                  strncat(sBlock.m_ssIndices[uIndexId].m_csFilename, pcFilename, GNARF_MAX_FILENAME);
                  sBlock.m_ssIndices[uIndexId].m_szLength   = sStat.st_size;
                  sBlock.m_ssIndices[uIndexId].m_szPosition = uNextData;
                  
                  // TODO check if this is the last index within the block
                  
                  eResult = gnarf_write_block(fdArchiveFile, &sBlock);
                }
                
              }
              
            }
            
          }
          
          
        } else
        {
          eResult = GE_ARCHIVE_INDEX_CORRUPT;
        }
      }
      /* --- END aquire the found index and attach the file --- */
      
      close(fdInputFile);
      
    } else
    {
      eResult = GE_UNABLE_TO_OPEN_INPUT;
    }
    
    close(fdArchiveFile);
    
  }  
  {
    eResult = GE_UNABLE_TO_OPEN_ARCHIVE;
  }
  return eResult;
}
