
# gnarf - the GNUish archiver of FrEsC :D #

## usage ##

    gnarf [OPERATION] [ARCHIVE] [FILES]
    
    with OPERATION being one of
    
    -n  or  --new   create new archive with name ARCHIVE
    -d  or  --dir   list contents of ARCHIVE
    -a  or  --add   add FILES to ARCHIVE
    -?              print short help
        
## building gnarf ##

  This program builds with gcc under Linux or Windows with Cygwin.

  If you don't have a working programming environment I recommend
  using the [Cloud9 online IDE](http://c9.io).

  build the application
  
    make all
  
  remove files created during build process
  
    make clean
  
  run some tests on the program
  
    make test
  
## license ##

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

