/*
 *  This file is part of gnarf.
 *
 *  gnarf is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  gnarf is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with gnarf.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef __GNARF_H_INCLUDED__
#define __GNARF_H_INCLUDED__

typedef enum GNARF_ERROR
{

  GE_NO_ERROR,
  GE_NULL_POINTER_ERROR,
  GE_IO_ERROR,
  GE_UNABLE_TO_CREATE_ARCHIVE,
  GE_UNABLE_TO_OPEN_ARCHIVE,
  GE_UNABLE_TO_OPEN_INPUT,
  GE_MAGIC_VALUE_CORRUPT,
  GE_ARCHIVE_INDEX_CORRUPT,
  GE_UNEXPECTED_WRITE_ERROR,
  GE_UNEXPECTED_EOF
  
} GNARF_ERROR;

GNARF_ERROR gnarf_new(const char *pcArchiveName);

GNARF_ERROR gnarf_dir(const char *pcArchiveName);

GNARF_ERROR gnarf_add(const char *pcArchiveName, const char *pcInputName);

#endif
